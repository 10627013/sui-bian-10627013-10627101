package sample;

import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.collections.ObservableArray;

import javax.annotation.Resource;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox ice;
    public ComboBox add;
    public ComboBox addd;

    List<String> str = new ArrayList<>();
    List<String> stradd = new ArrayList<>();
    public void initialize(URL location , ResourceBundle resources){
        str.add("雪糕");
        str.add("雪花冰");
        str.add("冰淇淋");

        stradd.add("棉花糖");
        stradd.add("仙草");
        stradd.add("粉圓");
        stradd.add("紅豆");
        stradd.add("綠豆");

        ice.setItems(FXCollections.observableArrayList(str));
        add.setItems(FXCollections.observableArrayList(stradd));
        addd.setItems(FXCollections.observableArrayList(stradd));
    }
}
